1
00:00:02,670 --> 00:00:04,620
My name is Dr. Kevin Stillwagon,

2
00:00:04,980 --> 00:00:06,990
my address is [deleted]

3
00:00:06,990 --> 00:00:09,480
[deleted] Clearwater Beach, Florida. I am a

4
00:00:09,480 --> 00:00:11,910
property owner and a taxpayer in Orange

5
00:00:11,910 --> 00:00:12,330
County.

6
00:00:14,190 --> 00:00:16,590
You are making some really bad decisions

7
00:00:16,590 --> 00:00:19,860
based on fear of a virus that

8
00:00:19,860 --> 00:00:22,410
has about a ninety-nine percent survival

9
00:00:22,410 --> 00:00:24,690
rate for most of us. That is

10
00:00:24,690 --> 00:00:27,960
unsubstantiated fear. So let me give you

11
00:00:27,960 --> 00:00:29,760
a couple of things to truly be

12
00:00:29,760 --> 00:00:33,510
fearful about. That mask that you

13
00:00:33,600 --> 00:00:36,750
keep insisting that people wear decreases the

14
00:00:36,750 --> 00:00:38,700
amount of oxygen in your lung tissue.

15
00:00:39,630 --> 00:00:41,640
We now know that this virus uses

16
00:00:41,640 --> 00:00:45,360
something called a furin cleavage site to

17
00:00:45,360 --> 00:00:48,090
merge with your lung tissue to infect

18
00:00:48,090 --> 00:00:51,720
you. And it works better with decreased

19
00:00:51,720 --> 00:00:53,760
oxygen. And peer reviewed

20
00:00:53,760 --> 00:00:56,100
research clearly shows that wearing a

21
00:00:56,100 --> 00:00:59,518
mask increases your chances of developing an

22
00:00:59,518 --> 00:01:03,150
upper respiratory infection thirteen times more than

23
00:01:03,150 --> 00:01:06,120
a person not wearing a mask. So

24
00:01:06,120 --> 00:01:08,970
I would stop wearing a mask immediately

25
00:01:09,570 --> 00:01:10,440
if I were you.

26
00:01:11,580 --> 00:01:14,520
Secondly, this shot that you insist on

27
00:01:14,520 --> 00:01:17,610
people getting gives you absolutely no protection

28
00:01:17,640 --> 00:01:20,940
against infection. It is the innate immune

29
00:01:20,940 --> 00:01:23,730
system that protects you from infection by

30
00:01:23,730 --> 00:01:27,480
using dendritic cells T-cells and natural

31
00:01:27,480 --> 00:01:31,440
killer cells without antibodies ever becoming involved.

32
00:01:31,770 --> 00:01:34,380
This shot has one goal and that

33
00:01:34,380 --> 00:01:36,739
goal is to make antibodies. These antibodies

34
00:01:36,739 --> 00:01:39,810
circulate inside of you and cannot prevent

35
00:01:39,840 --> 00:01:42,840
an infection. They can only react to

36
00:01:42,840 --> 00:01:45,660
something that has already gotten inside of

37
00:01:45,660 --> 00:01:49,470
you. They cannot keep something out. The

38
00:01:49,470 --> 00:01:51,840
shot decreases the ability of your

39
00:01:51,840 --> 00:01:54,660
innate immune system to keep viruses out

40
00:01:54,930 --> 00:01:58,650
by sixty percent and a booster shot

41
00:01:58,680 --> 00:02:02,070
will reduce it even more. Even worse,

42
00:02:02,100 --> 00:02:04,470
the antibodies that are created by this

43
00:02:04,470 --> 00:02:08,310
shot can no longer neutralize variants and

44
00:02:08,310 --> 00:02:12,000
actually enhance the virus ability to infect

45
00:02:12,060 --> 00:02:15,630
you. It should be painfully obvious to

46
00:02:15,630 --> 00:02:18,870
you by now that fully vaccinated people

47
00:02:18,900 --> 00:02:22,290
are getting sick and this will continue

48
00:02:22,290 --> 00:02:24,270
to get worse if you keep trying

49
00:02:24,270 --> 00:02:27,120
to jab people while a virus is

50
00:02:27,120 --> 00:02:28,410
trying to spread.

51
00:02:29,730 --> 00:02:33,240
The variants are emerging from the vaccinated

52
00:02:33,240 --> 00:02:34,200
population.

53
00:02:36,660 --> 00:02:39,270
This so called vaccine is still being

54
00:02:39,270 --> 00:02:41,910
administered on what's called an emergency use

55
00:02:41,910 --> 00:02:46,410
authorization. It is not FDA

56
00:02:46,440 --> 00:02:47,250
approved.

57
00:02:48,780 --> 00:02:54,030
The FDA approved a biological licensing application

58
00:02:54,900 --> 00:02:57,150
for a product called "Comirnaty".

59
00:02:58,500 --> 00:03:01,710
The application was approved, not the product.

60
00:03:02,490 --> 00:03:05,550
Comirnaty is not available in the United

61
00:03:05,550 --> 00:03:06,270
States.

62
00:03:07,740 --> 00:03:12,480
Therefore, by law, you cannot force people

63
00:03:12,540 --> 00:03:15,960
to take this drug without informed consent

64
00:03:17,520 --> 00:03:19,860
and without animal trials to prove that

65
00:03:19,860 --> 00:03:20,640
it is safe.

66
00:03:22,170 --> 00:03:23,850
Mayor Demings [Jerry L. Demings], please look at me.

67
00:03:25,530 --> 00:03:28,470
You, Sir, are in violation of the

68
00:03:28,470 --> 00:03:32,130
United States Constitution and the Nuremberg Code.

69
00:03:32,430 --> 00:03:35,790
You will be held accountable. Good day.

70
00:03:37,029 --> 00:03:38,937
Alright, thank you for your comments. We

71
00:03:38,937 --> 00:03:40,410
will move to the next speaker, please.
