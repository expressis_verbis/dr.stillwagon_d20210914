1
00:00:02,670 --> 00:00:04,620
Mein Name ist Dr. Kevin Stillwagon,

2
00:00:04,980 --> 00:00:06,990
meinen Adresse ist [gelöscht]

3
00:00:06,990 --> 00:00:09,480
[gelöscht], Clearwater Beach, Florida. Ich bin

4
00:00:09,480 --> 00:00:11,910
Grundstücksbesitzer und Steuerzahler in

5
00:00:11,910 --> 00:00:12,330
Orange County.

6
00:00:14,190 --> 00:00:16,590
Sie treffen einige wirklch schlechte Entscheidungen

7
00:00:16,590 --> 00:00:19,860
aus Angst vor einem Virus, das

8
00:00:19,860 --> 00:00:22,410
etwa 99 % Überlebens-Chancen für die

9
00:00:22,410 --> 00:00:24,690
meisten von uns hat. Dies ist eine

10
00:00:24,690 --> 00:00:27,960
unbegründete Angst. Lassen Sie mich Ihnen

11
00:00:27,960 --> 00:00:29,760
einige Dinge nennen, vor denen Sie wirklich

12
00:00:29,760 --> 00:00:33,510
Angst haben sollten. Die Maske, auf deren

13
00:00:33,600 --> 00:00:36,750
Tragepflicht Sie bestehen, vermindert den

14
00:00:36,750 --> 00:00:38,700
Gehalt an Sauerstoff in Ihrem Lungengewebe.

15
00:00:39,630 --> 00:00:41,640
Wir wissen jetzt, dass sich dieses Virus etwas namens

16
00:00:41,640 --> 00:00:45,360
Furin-Spaltungsstelle bedient, um mit Ihrem

17
00:00:45,360 --> 00:00:48,090
Lungengewebe zu verschmelzen und Sie zu

18
00:00:48,090 --> 00:00:51,720
infizieren. Und das funktioniert besser bei weniger

19
00:00:51,720 --> 00:00:53,760
Sauerstoff. Und von Fachleuten überprüfte

20
00:00:53,760 --> 00:00:56,100
Studien zeigen klar, dass das Tragen von

21
00:00:56,100 --> 00:00:59,518
Masken die Wahrscheinlichkeit der Entstehung einer Infektion

22
00:00:59,518 --> 00:01:03,150
der oberen Atemwege um das Dreizehnfache erhöht gegenüber

23
00:01:03,150 --> 00:01:06,120
Personen, die keine Maske tragen. An

24
00:01:06,120 --> 00:01:08,970
Ihrer Stelle würde ich unverzüglich damit aufhören,

25
00:01:09,570 --> 00:01:10,440
eine Maske zu tragen.

26
00:01:11,580 --> 00:01:14,520
Zweitens, die Injektion, auf die Sie bestehen und die

27
00:01:14,520 --> 00:01:17,610
jeder bekommen soll, gibt Ihnen keinerlei Schutz

28
00:01:17,640 --> 00:01:20,940
vor Infektionen. Es ist das angeborene

29
00:01:20,940 --> 00:01:23,730
Immunsystem, das Sie mittels Dentritischen Zellen,

30
00:01:23,730 --> 00:01:27,480
T-Zellen und natürlichen Killerzellen schützt, ohne dass

31
00:01:27,480 --> 00:01:31,440
Antikörper je daran beteiligt wären.

32
00:01:31,770 --> 00:01:34,380
Diese Injektion hat ein einziges Ziel, und das Ziel

33
00:01:34,380 --> 00:01:36,739
ist, Antikörper zu produzieren. Diese Antikörper

34
00:01:36,739 --> 00:01:39,810
zirkulieren in Ihnen und können keine Infektion

35
00:01:39,840 --> 00:01:42,840
verhindern. Sie können nur auf etwas reagieren,

36
00:01:42,840 --> 00:01:45,660
das ohnehin schon in Ihr Inneres gelangt

37
00:01:45,660 --> 00:01:49,470
ist. Sie können kein Eindringen verhindern. Die

38
00:01:49,470 --> 00:01:51,840
Injektion verringert die Fähigkeit Ihres angeborenen Immunsystems,

39
00:01:51,840 --> 00:01:54,660
Viren am Eindringen zu hindern,

40
00:01:54,930 --> 00:01:58,650
um sechzig Prozent, und eine Verstärkerinjektion

41
00:01:58,680 --> 00:02:02,070
verringert das noch weiter. Es kommt noch schlimmer,

42
00:02:02,100 --> 00:02:04,470
die Antikörper, die durch diese Injektion gebildet

43
00:02:04,470 --> 00:02:08,310
werden, können keine Varianten mehr unschädlich machen,

44
00:02:08,310 --> 00:02:12,000
sondern verstärken noch das Virus in seiner Fähigkeit, Sie

45
00:02:12,060 --> 00:02:15,630
zu infizieren. Sie sollten inzwischen schmerzhaft erkennen,

46
00:02:15,630 --> 00:02:18,870
dass vollständig geimpfte Menschen

47
00:02:18,900 --> 00:02:22,290
krank werden, und es wird immer schlimmer werden,

48
00:02:22,290 --> 00:02:24,270
wenn Sie versuchen,

49
00:02:24,270 --> 00:02:27,120
Menschen zu spritzen, während ein Virus versucht,

50
00:02:27,120 --> 00:02:28,410
sich auszubreiten.

51
00:02:29,730 --> 00:02:33,240
Die Varianten bilden sich aus der geimpften Bevölkerung

52
00:02:33,240 --> 00:02:34,200
heraus.

53
00:02:36,660 --> 00:02:39,270
Der sogenannte Impfstoff wird immer noch

54
00:02:39,270 --> 00:02:41,910
im Rahmen der sogenannten Notfallzulassung

55
00:02:41,910 --> 00:02:46,410
angewendet. Er ist nicht durch die FDA

56
00:02:46,440 --> 00:02:47,250
zugelassen.

57
00:02:48,780 --> 00:02:54,030
Die FDA hat einem Antrag für den Versand eines

58
00:02:54,900 --> 00:02:57,150
biologisches Präparats namens "Comirnaty" stattgegeben.

59
00:02:58,500 --> 00:03:01,710
Der Antrag wurde bewilligt, nicht das Produkt.

60
00:03:02,490 --> 00:03:05,550
Comirnaty ist in den Vereinigten Staaten nicht

61
00:03:05,550 --> 00:03:06,270
erhältlich.

62
00:03:07,740 --> 00:03:12,480
Aus diesem Grund können Sie von Gesetzes wegen

63
00:03:12,540 --> 00:03:15,960
Menschen nicht zwingen, dieses Mittel ohne Einverständniserklärung

64
00:03:17,520 --> 00:03:19,860
einzunehmen und ohne Tierversuche, die beweisen, dass

65
00:03:19,860 --> 00:03:20,640
es sicher ist.

66
00:03:22,170 --> 00:03:23,850
Bürgermeister Demings [Jerry L. Demings], schauen Sie mich an.

67
00:03:25,530 --> 00:03:28,470
Sie, verletzen die Verfassung der Vereinigten Saaten

68
00:03:28,470 --> 00:03:32,130
und den Nürnberger Kodex.

69
00:03:32,430 --> 00:03:35,790
Sie werden dafür zur Rechenschaft gezogen. Guten Tag.

70
00:03:37,029 --> 00:03:38,937
In Ordnung, danke für Ihren Beitrag. Bitte,

71
00:03:38,937 --> 00:03:40,410
machen wir mit dem nächsten Sprecher weiter.
