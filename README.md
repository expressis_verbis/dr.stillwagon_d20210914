# Subtitles to Dr. Stillwagon's public comment (2021-09-14)

Subtitles to Dr. Stillwagon's public comment before the Orange County Florida Board of County Commissioners, appearantly held on 2021-09-14, about them insisting on masks and jabs and thus violating the United States Constitution as well as the Nuremberg Code.

* based on: [dr-stillwagons-public-statement-on-09-14-2021.mp4](http://allthatstreaming.com/media/ats/video/dr-stillwagons-public-statement-on-09-14-2021.mp4)

* transcript found at [Orange County Florida Board of County Commissioners Public Comment video Transcription 9.26.21.pdf](http://www.padrak.com/coronavirus/Orange%20County%20Florida%20Board%20of%20County%20Commissioners%20Public%20Comment%20video%20Transcription%209.26.21.pdf)

* tools used: [ffmpeg.org](https://ffmpeg.org/), [alphacep/vosk-server](https://github.com/alphacep/vosk-server), brain and memory, and [dict.leo.org](https://dict.leo.org/englisch-deutsch/)

